<!doctype html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>{{ config('app.name', 'ION METER') }}</title>
    <!-- Scripts -->
    <script src="{{ asset('/js/app.js') }}"></script>
    <script>
        var laravel = @json([ 'baseURL' => url('/'), 'csrfToken' => csrf_token()  ])
    </script>

    <script>
        window._asset = '{{ asset('') }}';
        window._locale = "{{ app()->getLocale() }}";
    </script>
    <!-- Fonts -->
    <link rel="dns-prefetch" href="//fonts.gstatic.com">
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">
    <link rel="preconnect" href="https://fonts.gstatic.com">
    <link rel="preconnect" href="https://fonts.gstatic.com">
    <link href="https://fonts.googleapis.com/css2?family=Tajawal:wght@200;300;400;500;700;800;900&display=swap" rel="stylesheet">
    <!-- Styles -->

    <link href="{{ asset('css/app.css') }}" rel="stylesheet">
      <link rel="stylesheet" href="{{ asset('css/style.css') }}" type="text/css">
</head>
<body>

<div id="app" class="mainapp">
    <router-view></router-view>

</div>
</body>
</html>
