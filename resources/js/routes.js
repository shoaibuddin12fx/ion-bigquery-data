import SqliteService from "./services/sqlite.service";
import NetworkService from "./services/network.service";
import UtilityService from "./services/utility.service";

let sqlite = new SqliteService();
let network = new NetworkService();
let utility = new UtilityService();
// routes js to maintain

// Dashboard
// Containers
const TheContainer = () => import('./dashboard/containers/TheContainer')

// Views
const Dashboard = () => import('./dashboard/views/Dashboard')

const Colors = () => import('./dashboard/views/theme/Colors')
const Typography = () => import('./dashboard/views/theme/Typography')

const Charts = () => import('./dashboard/views/charts/Charts')
const Widgets = () => import('./dashboard/views/widgets/Widgets')

// Views - Components
const Cards = () => import('./dashboard/views/base/Cards')
const Forms = () => import('./dashboard/views/base/Forms')
const Switches = () => import('./dashboard/views/base/Switches')
const Tables = () => import('./dashboard/views/base/Tables')
const Tabs = () => import('./dashboard/views/base/Tabs')
const Breadcrumbs = () => import('./dashboard/views/base/Breadcrumbs')
const Carousels = () => import('./dashboard/views/base/Carousels')
const Collapses = () => import('./dashboard/views/base/Collapses')
const Jumbotrons = () => import('./dashboard/views/base/Jumbotrons')
const ListGroups = () => import('./dashboard/views/base/ListGroups')
const Navs = () => import('./dashboard/views/base/Navs')
const Navbars = () => import('./dashboard/views/base/Navbars')
const Paginations = () => import('./dashboard/views/base/Paginations')
const Popovers = () => import('./dashboard/views/base/Popovers')
const ProgressBars = () => import('./dashboard/views/base/ProgressBars')
const Tooltips = () => import('./dashboard/views/base/Tooltips')

// Views - Buttons
const StandardButtons = () => import('./dashboard/views/buttons/StandardButtons')
const ButtonGroups = () => import('./dashboard/views/buttons/ButtonGroups')
const Dropdowns = () => import('./dashboard/views/buttons/Dropdowns')
const BrandButtons = () => import('./dashboard/views/buttons/BrandButtons')

// Views - Icons
const CoreUIIcons = () => import('./dashboard/views/icons/CoreUIIcons')
const Brands = () => import('./dashboard/views/icons/Brands')
const Flags = () => import('./dashboard/views/icons/Flags')

// Views - Notifications
const Alerts = () => import('./dashboard/views/notifications/Alerts')
const Badges = () => import('./dashboard/views/notifications/Badges')
const Modals = () => import('./dashboard/views/notifications/Modals')

// Views - Pages
const Page404 = () => import('./dashboard/views/pages/Page404')
const Page500 = () => import('./dashboard/views/pages/Page500')
const Login = () => import('./dashboard/views/pages/Login')
const ChangePassword = () => import('./dashboard/views/pages/ChangePassword')
const Register = () => import('./dashboard/views/pages/Register')

// Users
const Users = () => import('./dashboard/views/users/Users')
const User = () => import('./dashboard/views/users/User')



async function authGuard(to, from, next) {
    var isAuthenticated = false;
    //this is just an example. You will have to find a better or
    // centralised way to handle you localstorage data handling
    let user = await sqlite.getActiveUser();
    if (!user) {
        next('/'); // allow to enter route
    }else{
        next();
    }
}

async function inverseAuthGuard(to, from, next) {
    var isAuthenticated = false;
    //this is just an example. You will have to find a better or
    // centralised way to handle you localstorage data handling
    let user = await sqlite.getActiveUser();
    if (!user) {
        next(); // allow to enter route
    } else {
        console.log(user);
        next('/dashboard'); // go to '/login';
    }
}


export const routes = [

    { path: '/', name: 'Login', component: Login, beforeEnter: inverseAuthGuard  },
    { path: '/change-password/:id', name: 'ChangePassword', component: ChangePassword },
    // Dashboard
    {
        path: '/dashboard',
        redirect: '/admin',
        name: 'Home',
        component: TheContainer,
        beforeEnter: authGuard,
        children: [
            {
                path: '/admin',
                name: 'Dashboard',
                component: Dashboard
            },
            {
                path: '/user',
                redirect: '/admin/all-users',
                name: 'User',
                component: {
                    render(c) { return c('router-view') }
                },
                children: [
                    {
                        path: '/admin/all-users',
                        name: 'All Users',
                        component: require('./dashboard/pages/user-pages/AllUsers').default
                    },
                    {
                        path: '/admin/add-user',
                        name: 'Add User',
                        component: require('./dashboard/pages/user-pages/AddUser').default
                    },
                    {
                        path: '/admin/all-users/:id',
                        name: 'View User',
                        component: require('./dashboard/pages/user-pages/SingleUser').default
                    },
                    {
                        path: '/admin/update-users/:id',
                        name: 'Update User',
                        component: require('./dashboard/pages/user-pages/UpdateUser').default
                    },
                    {
                        path: '/admin/all-account-types',
                        name: 'All Roles',
                        component: require('./dashboard/pages/role-pages/AllRoles').default
                    },
                    {
                        path: '/admin/add-account-type',
                        name: 'Add Role',
                        component: require('./dashboard/pages/role-pages/AddRole').default
                    },
                    {
                        path: '/admin/all-account-types/:id',
                        name: 'View Role',
                        component: require('./dashboard/pages/role-pages/SingleRole').default
                    },
                    {
                        path: '/admin/update-account-types/:id',
                        name: 'Update Role',
                        component: require('./dashboard/pages/role-pages/UpdateRole').default
                    },
                ]
            },
            {
                path: '/admin/all-properties',
                name: 'All Properties',
                component: require('./dashboard/pages/property-pages/AllProperties').default
            },
            {
                path: '/admin/add-property',
                name: 'Add User',
                component: require('./dashboard/pages/property-pages/AddProperty').default
            },
            {
                path: '/admin/all-properties/:id',
                name: 'View User',
                component: require('./dashboard/pages/property-pages/SingleProperty').default
            },
            {
                path: '/admin/update-properties/:id',
                name: 'Update User',
                component: require('./dashboard/pages/property-pages/UpdateProperty').default
            },

            {
                path: '/widgets',
                name: 'Widgets',
                component: Widgets
            },
            {
                path: '/users',
                meta: {
                    label: 'Users'
                },
                component: {
                    render(c) {
                        return c('router-view')
                    }
                },
                children: [
                    {
                        path: '',
                        name: 'Users',
                        component: Users
                    },
                    {
                        path: ':id',
                        meta: {
                            label: 'User Details'
                        },
                        name: 'User',
                        component: User
                    }
                ]
            },
            {
                path: 'base',
                redirect: '/base/cards',
                name: 'Base',
                component: {
                    render(c) { return c('router-view') }
                },
                children: [
                    {
                        path: '/base/cards',
                        name: 'Cards',
                        component: Cards
                    },
                    {
                        path: '/base/forms',
                        name: 'Forms',
                        component: Forms
                    },
                    {
                        path: '/base/switches',
                        name: 'Switches',
                        component: Switches
                    },
                    {
                        path: '/base/tables',
                        name: 'Tables',
                        component: Tables
                    },
                    {
                        path: '/base/tabs',
                        name: 'Tabs',
                        component: Tabs
                    },
                    {
                        path: '/base/breadcrumbs',
                        name: 'Breadcrumbs',
                        component: Breadcrumbs
                    },
                    {
                        path: '/base/carousels',
                        name: 'Carousels',
                        component: Carousels
                    },
                    {
                        path: '/base/collapses',
                        name: 'Collapses',
                        component: Collapses
                    },
                    {
                        path: '/base/jumbotrons',
                        name: 'Jumbotrons',
                        component: Jumbotrons
                    },
                    {
                        path: '/base/list-groups',
                        name: 'List Groups',
                        component: ListGroups
                    },
                    {
                        path: '/base/navs',
                        name: 'Navs',
                        component: Navs
                    },
                    {
                        path: '/base/navbars',
                        name: 'Navbars',
                        component: Navbars
                    },
                    {
                        path: '/base/paginations',
                        name: 'Paginations',
                        component: Paginations
                    },
                    {
                        path: '/base/popovers',
                        name: 'Popovers',
                        component: Popovers
                    },
                    {
                        path: '/base/progress-bars',
                        name: 'Progress Bars',
                        component: ProgressBars
                    },
                    {
                        path: '/base/tooltips',
                        name: 'Tooltips',
                        component: Tooltips
                    }
                ]
            },
            {
                path: 'buttons',
                redirect: '/buttons/standard-buttons',
                name: 'Buttons',
                component: {
                    render(c) { return c('router-view') }
                },
                children: [
                    {
                        path: '/buttons/standard-buttons',
                        name: 'Standard Buttons',
                        component: StandardButtons
                    },
                    {
                        path: '/buttons/button-groups',
                        name: 'Button Groups',
                        component: ButtonGroups
                    },
                    {
                        path: '/buttons/dropdowns',
                        name: 'Dropdowns',
                        component: Dropdowns
                    },
                    {
                        path: '/buttons/brand-buttons',
                        name: 'Brand Buttons',
                        component: BrandButtons
                    }
                ]
            },
            {
                path: 'icons',
                redirect: '/icons/coreui-icons',
                name: 'CoreUI Icons',
                component: {
                    render(c) { return c('router-view') }
                },
                children: [
                    {
                        path: '/icons/coreui-icons',
                        name: 'Icons library',
                        component: CoreUIIcons
                    },
                    {
                        path: '/icons/brands',
                        name: 'Brands',
                        component: Brands
                    },
                    {
                        path: '/icons/flags',
                        name: 'Flags',
                        component: Flags
                    }
                ]
            },
            {
                path: 'notifications',
                redirect: '/notifications/alerts',
                name: 'Notifications',
                component: {
                    render(c) { return c('router-view') }
                },
                children: [
                    {
                        path: 'alerts',
                        name: 'Alerts',
                        component: Alerts
                    },
                    {
                        path: 'badges',
                        name: 'Badges',
                        component: Badges
                    },
                    {
                        path: 'modals',
                        name: 'Modals',
                        component: Modals
                    }
                ]
            }
        ]
    },
    {
        path: '/pages',
        redirect: '/pages/404',
        name: 'Pages',
        component: {
            render(c) { return c('router-view') }
        },
        children: [
            {
                path: '*',
                name: 'Page404',
                component: Page404
            },
            {
                path: '/pages/500',
                name: 'Page500',
                component: Page500
            },
            {
                path: '/pages/login',
                name: 'Login',
                component: Login
            },
            {
                path: '/pages/register',
                name: 'Register',
                component: Register
            }
        ]
    }

];
