/**
 * First we will load all of this project's JavaScript dependencies which
 * includes Vue and other libraries. It is a great starting point when
 * building robust, powerful web applications using Vue and Laravel.
 */

import VueRouter from "vue-router";
import VueMq from 'vue-mq';
import VueI18n from 'vue-i18n'
import SqliteService from "./services/sqlite.service";
import Vue from 'vue';
import { BootstrapVue, IconsPlugin } from 'bootstrap-vue'
import { BootstrapVueIcons } from 'bootstrap-vue'
import 'bootstrap-vue/dist/bootstrap-vue-icons.min.css'
import OtpInput from "@bachdgvn/vue-otp-input";
import VueToast from 'vue-toast-notification';
import 'vue-toast-notification/dist/theme-sugar.css';
import VueSession from 'vue-session';
import Chartist from "chartist";
import CoreuiVue from '@coreui/vue'
import { iconsSet as icons } from './dashboard/assets/icons/icons.js'
import store from './store'

Vue.config.performance = true
Vue.use(CoreuiVue)
Vue.prototype.$log = console.log.bind(console)

require('./bootstrap');


Vue.use(BootstrapVue)
Vue.use(BootstrapVueIcons)
Vue.component("v-otp-input", OtpInput);
Vue.use(VueToast, {
    // One of the options
    position: 'top'
})
window.Vue = require('vue').default;
Vue.use(VueRouter);
Vue.use(VueI18n);
Vue.use(VueSession)
/**
 * The following block of code may be used to automatically register your
 * Vue components. It will recursively scan this directory for the Vue
 * components and automatically register them with their "basename".
 *
 * Eg. ./components/ExampleComponent.vue -> <example-component></example-component>
 */
let en = require('../lang/en.json');
let ar = require('../lang/ar.json');
var messages = {
    "en": en,
    "ar": ar
} // require('./config/lang.json');
const i18n = new VueI18n({
    locale: 'en', // set locale
    messages, // set locale messages
})
// const files = require.context('./', true, /\.vue$/i)
// files.keys().map(key => Vue.component(key.split('/').pop().split('.')[0], files(key).default))
require("./components");

/**
 * Next, we will create a fresh Vue application instance and attach it to
 * the page. Then, you may begin adding components to this application
 * or customize the JavaScript scaffolding to fit your unique needs.
 */

import { routes } from "./routes";

const router = new VueRouter({
    base: '/ion-bigquery-data/public/',
    routes: routes,
    mode: "history",
    linkActiveClass: 'nav-item active',
    scrollBehavior() {
        return { x: 0, y: 0 };
    },
})

window.onload = function () {
    const app = new Vue({
        el: '#app',
        router,
        store,
        icons,
    });
}


let sqlite = new SqliteService();
import Multiselect from 'vue-multiselect'
Vue.component('multiselect', Multiselect);

sqlite.initialize().then(v => {
    window.onload = function () {
        const app = new Vue({
            el: '#app',
            components: { Multiselect },
            i18n,
            router,
            store,
            icons,
        });
    }
});
