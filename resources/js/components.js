
Vue.component('sign-in-page', require('./pages/signin-page/SignInPageComponent').default);


//Dashbaord Components

//User components
Vue.component('AddUser', require('./dashboard/pages/user-pages/AddUser').default);
Vue.component('AllUsers', require('./dashboard/pages/user-pages/AllUsers').default);
Vue.component('SingleUser', require('./dashboard/pages/user-pages/SingleUser').default);
Vue.component('UpdateUser', require('./dashboard/pages/user-pages/UpdateUser').default);
//Role components
Vue.component('AddRoles', require('./dashboard/pages/role-pages/AddRole').default);
Vue.component('AllRoles', require('./dashboard/pages/role-pages/AllRoles').default);
Vue.component('SingleRole', require('./dashboard/pages/role-pages/SingleRole').default);
Vue.component('UpdateRole', require('./dashboard/pages/role-pages/UpdateRole').default);
//Property components
Vue.component('AllProperties', require('./dashboard/pages/property-pages/AllProperties').default);
Vue.component('AddProperty', require('./dashboard/pages/property-pages/AddProperty').default);
Vue.component('SingleProperty', require('./dashboard/pages/property-pages/SingleProperty').default);
Vue.component('UpdateProperty', require('./dashboard/pages/property-pages/UpdateProperty').default);




