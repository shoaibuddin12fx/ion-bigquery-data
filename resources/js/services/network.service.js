import ApiService from "./api.service";
const apiService = new ApiService();
import UtilityService from "./utility.service";
const utilityService = new UtilityService();
import SqliteService from "./sqlite.service";
const sqliteService = new SqliteService();


export default class NetworkService {

    constructor() { }


    addUser(data) {
        return this.axiosPostResponse('/admin-user', data, null)
    }

    logInUser(data) {
        return this.axiosPostResponse('/login', data)
    }
    allUsers(){
        return this.axiosGetResponse('/admin-user', null, false);
    }
    singleUser(id) {
        return this.axiosGetResponse('/admin-user', id = id);
    }
    UpdateUser(data,id) {
        return this.axiosPutResponse('/admin-user', data, id=id);
    }
    deleteUser(id) {
        return this.axiosDeleteResponse('/admin-user', id = id);
    }

    //Roles
    allRoles() {
        return this.axiosGetResponse('/admin-role', null, false);
    }
    singleRole(id) {
        return this.axiosGetResponse('/admin-role', id = id);
    }
    addRole(data){
        return this.axiosPostResponse('/admin-role', data, null)
    }
    UpdateRole(data, id) {
        return this.axiosPutResponse('/admin-role', data, id = id);
    }
    deleteRole(id) {
        return this.axiosDeleteResponse('/admin-role', id = id);
    }
    allProperties(){
        return this.axiosGetResponse('/property', null, false);
    }
    singleProperty(id) {
        return this.axiosGetResponse('/property', id = id);
    }
    addProperty(data){
        return this.axiosPostResponse('property', data, null)
    }
    UpdateProperty(data, id) {
        return this.axiosPutResponse('/property', data, id = id);
    }
    deleteProperty(id) {
        return this.axiosDeleteResponse('/property', id = id);
    }
    // getCompleteProfile() {
    //     return this.axiosGetResponse('user/get_complete_profile', null, false)
    // }

    // forgetPassword(data) {
    //     return this.axiosPostResponse('user/forget_password', data)
    // }

    // newPassword(data) {
    //     return this.axiosPostResponse('user/new_password', data)
    // }

    // updateProfile(data) {
    //     return this.axiosPostResponse('user/profile', data)
    // }

    // updateUserProfile(data) {
    //     console.log({ data })
    //     return this.axiosPostResponse('user/updateUserProfile', data)
    // }

    generateCode(data) {

        return this.axiosPostResponse('user/generate_code', data)
    }

    getUserRole() {
        return this.axiosGetResponse('user/get_user_role', null, false);
    }

    getRoles() {
        return this.axiosGetResponse('/user-roles', null, false);
    }

    sendUserUpdatedData(data) {
        return this.axiosPostResponse('user/change_email_or_contact', data);
    }

    sendUpdatePassword(data) {
        return this.axiosPostResponse('user/change_password', data);
    }

    axiosGetResponse(key, id = null, showLoader = false, showError = true, contentType = 'application/json') {
        return this.httpResponse('get', key, {}, id, showLoader, showError, contentType);
    }

    axiosPostResponse(key, data, id = null, showLoader = false, showError = true, contentType = 'application/json') {
        return this.httpResponse('post', key, data, id, showLoader, showError, contentType);
    }

    // axiosPostLoginResponse(key, data, id = null, showLoader = false, showError = true, contentType = 'application/json') {
    //     return this.httpResponse('post', key, data, id, showLoader, showError, contentType);
    // }

    axiosPutResponse(key, data, id = null, showloader = false, showError = true, contentType = 'application/json') {
        return this.httpResponse('put', key, data, id, showloader, showError, contentType);
    }
    axiosDeleteResponse(key, id = null, showloader = false, showError = true, contentType = 'application/json') {
        return this.httpResponse('delete', key, {}, id, showloader, showError, contentType);
    }




    // axiosPostResponse(api, data, config = {}) {
    //     return apiService.post(api, data, config);
    // }





    httpResponse(type = 'get', key, data, id = null, showLoader = false, showError = true, contentType = 'application/json') {

        return new Promise((resolve, reject) => {

            if (showLoader == true) {
                utilityService.showLoader();
            }

            const _id = (id) ? '/' + id : '';
            const url = key + _id;
            console.log({ url, _id });

            // let headers = {
            //     'Authorization': 'Bearer' + sqliteService.getToken()
            // }
            const seq = (type == 'get') ? apiService.get(url) : ((type == 'put') ? apiService.put(url, data) : ((type=='delete') ? apiService.delete(url) : apiService.post(url, data)));
            console.log({ seq });
            seq.then((res) => {
                console.log("res..",{ res });

                if (res.status != 200) {
                    if (showError == true) {
                        utilityService.presentFailureToast(res['message']);
                    }

                    reject(null);
                    return;
                }

                resolve(res.data);
                // this.utility.presentSuccessToast(res['message']);

            }, (err) => {

                let error = err;
                console.log(error);

                if (showLoader == true) {
                    utilityService.hideLoader();
                }

                if (showError == true) {
                    // utilityService.presentFailureToast(error['message']);
                }

                console.log({ err });

                reject(err);

            })

        });

    }
}
