import SqliteService from "./sqlite.service";

var json = require('../../js/config/config.json');
const sqliteService = new SqliteService();



export default class ApiService {

    // constructor() {
    //     json.base_url = 'api/'


    // }

    get(api, config = {}) {
        console.log({ api })
        return new Promise((resolve, reject) => {
            window.axios.get(json.base_url + api, config).then(res => {
                resolve(res.data);
            }, (err) => {
                reject(err.response.data);
            })
                ;
        })

    }

    post(api, data, config = {}) {
        return new Promise((resolve, reject) => {
            window.axios.post(json.base_url + api, data, config).then(res => {
                resolve(res.data);
            }, (err) => {
                reject(err.response.data);
            })
        })
    }

    // postLogin(api, data, config = {}) {
    //     return new Promise( resolve => {
    //         window.axios.post(json.base_url+api, data, config).then(res => {
    //             resolve(res.data);
    //         });
    //     })
    // }

    put(api, data, config = {}) {
        console.log({ config })
        return new Promise((resolve, reject) => {
            window.axios.put(json.base_url + api, data, config).then(res => {
                resolve(res.data);
            }, (err) => {
                reject(err.response.data);
            })
        })
    }
    delete(api, config = {}) {
        return window.axios.delete(json.base_url + api, config);
    }




}
