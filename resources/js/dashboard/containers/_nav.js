export default [
    {
        _name: 'CSidebarNav',
        _children: [
            {
                _name: 'CSidebarNavItem',
                name: 'Dashboard',
                to: '/dashboard',
                icon: 'cil-speedometer',
                badge: {
                    color: 'primary',
                    text: 'NEW'
                }
            },
            {
                _name: 'CSidebarNavTitle',
                _children: ['user']
            },
            {
                _name: 'CSidebarNavItem',
                name: 'Users',
                to: '/admin/all-users',
                icon: 'cil-list'
            },
            {
                _name: 'CSidebarNavItem',
                name: 'Account Type',
                to: '/admin/all-account-types',
                icon: 'cil-settings'
            },
          
            {
                _name: 'CSidebarNavTitle',
                _children: ['Property']
            },
            {
                _name: 'CSidebarNavItem',
                name: 'Properties',
                to: '/admin/all-properties',
                icon: 'cil-home'
            },
            {
                _name: 'CSidebarNavDropdown',
                name: 'Icons',
                route: '/icons',
                icon: 'cil-star',
                items: [
                    {
                        name: 'CoreUI Icons',
                        to: '/icons/coreui-icons',
                        badge: {
                            color: 'info',
                            text: 'NEW'
                        }
                    },
                    {
                        name: 'Brands',
                        to: '/icons/brands'
                    },
                    {
                        name: 'Flags',
                        to: '/icons/flags'
                    }
                ]
            },
            {
                _name: 'CSidebarNavDropdown',
                name: 'Notifications',
                route: '/notifications',
                icon: 'cil-bell',
                items: [
                    {
                        name: 'Alerts',
                        to: '/notifications/alerts'
                    },
                    {
                        name: 'Badges',
                        to: '/notifications/badges'
                    },
                    {
                        name: 'Modals',
                        to: '/notifications/modals'
                    }
                ]
            },
            {
                _name: 'CSidebarNavItem',
                name: 'Widgets',
                to: '/widgets',
                icon: 'cil-calculator',
                badge: {
                    color: 'primary',
                    text: 'NEW',
                    shape: 'pill'
                }
            },
            {
                _name: 'CSidebarNavDivider',
                _class: 'm-2'
            },
            {
                _name: 'CSidebarNavTitle',
                _children: ['Extras']
            },
            {
                _name: 'CSidebarNavDropdown',
                name: 'Pages',
                route: '/pages',
                icon: 'cil-star',
                items: [
                    {
                        name: 'Login',
                        to: '/pages/login'
                    },
                    {
                        name: 'Register',
                        to: '/pages/register'
                    },
                    {
                        name: 'Error 404',
                        to: '/pages/404'
                    },
                    {
                        name: 'Error 500',
                        to: '/pages/500'
                    }
                ]
            },
            {
                _name: 'CSidebarNavItem',
                name: 'Download CoreUI',
                href: 'http://coreui.io/vue/',
                icon: { name: 'cil-cloud-download', class: 'text-white' },
                _class: 'bg-success text-white',
                target: '_blank'
            },
            {
                _name: 'CSidebarNavItem',
                name: 'Try CoreUI PRO',
                href: 'http://coreui.io/pro/vue/',
                icon: { name: 'cil-layers', class: 'text-white' },
                _class: 'bg-danger text-white',
                target: '_blank'
            }
        ]
    }
]