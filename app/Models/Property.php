<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Property extends Model
{
    use HasFactory;

    protected $table = 'properties';
    protected $primaryKey = 'id';
    protected $fillable = [
        'property_id', 'street', 'city', 'st', 'zipcode', 'office_contact_number', 'contact', 'contact_email', 'manager_name', 'manager_email', 'manager_phone_number', 'maintainance_name', 'maintainance_email','maintainance_phone_number','maintainance_superviser_name','maintainance_superviser_email','maintainance_superviser_contact','property','developer','units','target_adc'
    ];

}
