<?php

namespace App\Http\Controllers\Api\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Property;
use App\Http\Resources\PropertyResource;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Str;
use App\Mail\InviteViaEmail;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;


class PropertyController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $Properties = Property::all();
        return self::success('Retreived Successfully!', ['data' => PropertyResource::collection($Properties)]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $data = $request->all();
        $validator = Validator::make($data, [
            'property_id' => 'required|string',
            'zipcode' => 'required|string',
        ]);

        if ($validator->fails()) {
            return self::failure($validator->errors()->first());
        }

        $Property = new Property($data);
        $Property->save();
        $Property = new PropertyResource($Property);
        $response = ['Property' => $Property];
        return self::success("Property is created Successfully", ['data' => $response]);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $Property = Property::find($id);

        if (!$Property) {
            return response()->json([
                'message' => 'Could not find the Property',
                'code' => 404
            ], 404);
        }
        $Property = new PropertyResource($Property);
        $response = ['Property' => $Property];
        return self::success("Property found", ['data' => $response]);
    }


    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $data = $request->all();
        $Property = Property::find($id);

        if (!$Property) {
            return response()->json([
                'message' => 'Could not find the Property',
                'code' => 404
            ], 404);
        }

        $validator = $this->validator_update($data);
        if ($validator->fails()) {
            return self::failure($validator->errors()->first());
        }
        if ($Property->update($data)) {
            $Property = new PropertyResource($Property);
            $response = ['Property' => $Property];
            return self::success("Property is updated Successfully", ['data' => $response]);
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $Property = Property::destroy($id);
        if ($Property == 0)
            return response()->json([
                'message' => 'Could not find the Property',
                'code' => 404
            ], 404);

        return self::success("Property has been deleted successfully!", []);
    }

    /** Validator update **/
    private function validator_update($data)
    {
        $rules = array();

        if (array_key_exists('name', $data)) {
            $rules['name'] = 'required|string';
        }

        return Validator::make(
            $data,
            $rules
        );
    }
}
