<?php

namespace App\Http\Controllers\Api\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\User;
use App\Models\Role;
use App\Http\Resources\AdminUserResource;
use App\Http\Resources\UserTableResource;
use App\Http\Resources\AdminRoleResource;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Str;
use App\Mail\InviteViaEmail;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;


class AdminUserController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        // $users = User::all();
        $users = DB::table('users')
        ->join('roles','users.role_id','=','roles.id')
        ->select('users.id','users.first_name','users.last_name','users.email','users.address','users.postal','users.country','users.city','users.state','users.role_id','roles.name as role', 'users.created_at', 'users.updated_at')
        ->where('users.id', '!=', 1)
        ->get();
        return self::success('Retreived Successfully!', ['data' => UserTableResource::collection($users)]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'first_name' => 'required|alpha',
            'last_name' => 'required|alpha',
            'email' => 'required|email|unique:users',
            'role_id' => 'required',
        ]);
        $data = json_encode($validator->errors());

        if($validator->fails())
           return self::failure($validator->errors()->first());
         //return $this->sendResponse($validate->errors(),'Missing Attributes',false);
         if (!$validator->fails()) {
                $validatePassword = Validator::make($request->all(),[
                    'password' => 'required|min:8|max:16'
                ]);
                if($validatePassword->fails())
                     return self::failure($validatePassword->errors('password'));

                $user = new User();
                $user->role_id =  $request->role_id;
                $user->first_name = $request->first_name;
                $user->last_name = $request->last_name;
                $user->email = $request->email;
                $user->password = Hash::make($request->password);
                $user->temp_password = $request->password;
                $user->country = $request->country;
                $user->address = $request->street_address;
                $user->city = $request->city;
                $user->state = $request->state;
                $user->postal = $request->zip;
                $user->remember_token = Str::random(100);
                if($user->save()){
                $user->role = Role::find($user->role_id)->name;
                $token = $user->createToken('My Web App')->accessToken;
                $user= new AdminUserResource($user);
                $response = ['token' => $token, 'user' => $user];
                //\Mail::to($request->email)->send(new InviteViaEmail($details));
                return self::success("User created successfully!", ['data' => $response]);
                }

    }
}

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $user = User::find($id);

        if (!$user) {
            return response()->json([
                'message' => 'Could not find the User',
                'code' => 404
            ], 404);
        }
        $user->role = Role::find($user->role_id)->name;
        $user = new AdminUserResource($user);
        $response = ['user' => $user];
        return self::success("User found", ['data' => $response]);
    }


    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $data = $request->all();

        $validator = $this->validator_update($data);
       if($validator->fails())
            return self::failure($validator->errors()->first());
        //return $this->sendResponse($validate->errors(),'Missing Attributes',false);
        if (!$validator->fails()) {
            $user = User::find($id);
            if($user === NULL){
            return response()->json([
                'message' => 'Could not find the User',
                'code' => 404
            ], 404);
        }
        if(array_key_exists("password", $data)){
            $data['temp_password'] = null;
            $data['password'] = Hash::make($data['password']);
            $data['password_confirmation'] = Hash::make($data['password_confirmation']);
           }
            if($user->update($data)){
                $user->role = Role::find($user->role_id)->name;
                $user = new AdminUserResource($user);
                $response = ['user' => $user];
                //\Mail::to($request->email)->send(new InviteViaEmail($details));
                return self::success("User has been updated successfully!", ['data' => $response]);
            }
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $user = User::destroy($id);
        if($user == 0)
        return response()->json([
            'message' => 'Could not find the User',
            'code' => 404
        ], 404);

        return self::success("User has been deleted successfully!", []);
    }

    /** Validator update **/
    private function validator_update($data)
    {
        $rules = array();

        if (array_key_exists('first_name', $data)) {
            $rules['first_name'] = 'required|alpha';
        }
        if (array_key_exists('password', $data)) {
            $rules['password'] = 'required|confirmed|min:8|max:16';
        }
        if (array_key_exists('last_name', $data)) {
            $rules['last_name'] = 'required|alpha';
        }
        if (array_key_exists('email', $data)) {
            $rules['email'] = 'required|email';
        }
        if (array_key_exists('address', $data)) {
            $rules['address'] = 'required';
        }

        return Validator::make(
            $data,
            $rules
        );
    }

    public function userRoles(){
        $roles = Role::all()->sortBy('id');
        $response = ['roles' => AdminRoleResource::collection($roles)];
        return self::success('All roles', ['data' => $response]);
    }
}

