<?php

namespace App\Http\Controllers\Api\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Role;
use App\Http\Resources\AdminRoleResource;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Str;
use App\Mail\InviteViaEmail;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;


class AdminRoleController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $roles = Role::all();
        return self::success('Retreived Successfully!', ['data' => AdminRoleResource::collection($roles)]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $data = $request->all();
        $validator = Validator::make($data, [
            'name' => 'required|string',
        ]);

        if ($validator->fails()) {
            return self::failure($validator->errors()->first());
        }

        $role = new Role($data);
        $role->save();
        $role = new AdminRoleResource($role);
        $response = ['role' => $role];
        return self::success("Role is created Successfully", ['data' => $response]);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $role = Role::find($id);

        if (!$role) {
            return response()->json([
                'message' => 'Could not find the role',
                'code' => 404
            ], 404);
        }
        $role = new AdminRoleResource($role);
        $response = ['role' => $role];
        return self::success("role found", ['data' => $response]);
    }


    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $data = $request->all();
        $role = Role::find($id);

        if (!$role) {
            return response()->json([
                'message' => 'Could not find the role',
                'code' => 404
            ], 404);
        }

        $validator = $this->validator_update($data);
        if ($validator->fails()) {
            return self::failure($validator->errors()->first());
        }
        if ($role->update($data)) {
            $role = new AdminRoleResource($role);
            $response = ['role' => $role];
            return self::success("role is updated Successfully", ['data' => $response]);
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $role = Role::destroy($id);
        if ($role == 0)
            return response()->json([
                'message' => 'Could not find the role',
                'code' => 404
            ], 404);

        return self::success("role has been deleted successfully!", []);
    }

    /** Validator update **/
    private function validator_update($data)
    {
        $rules = array();

        if (array_key_exists('name', $data)) {
            $rules['name'] = 'required|string';
        }

        return Validator::make(
            $data,
            $rules
        );
    }
}
