<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class PropertyResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     *
     */
    public function toArray($request)
    {
        return [
            // 'id' => $this->id,
            'property_id' => $this->property_id,
            'street' => $this->street,
            'city'=> $this->city,
            'st'=> $this->st,
            'zipcode'=> $this->zipcode,
            'office_contact_number'=> $this->office_contact_number,
            'contact'=> $this->contact,
            'contact_email'=> $this->contact_email,
            'manager_name'=> $this->manager_name,
            'manager_email'=> $this->manager_email,
            'manager_phone_number'=> $this->manager_phone_number,
            'maintainance_name'=> $this->maintainance_name,
            'maintainance_email'=> $this->maintainance_email,
            'maintainance_phone_number'=> $this->maintainance_phone_number,
            'maintainance_superviser_name'=> $this->maintainance_superviser_name,
            'maintainance_superviser_email'=> $this->maintainance_superviser_email,
            'maintainance_superviser_contact'=> $this->maintainance_superviser_contact,
            'property'=> $this->property,
            'developer'=> $this->developer,
            'units'=> $this->units,
            'target_adc'=> $this->target_adc,
        ];
    }
}
