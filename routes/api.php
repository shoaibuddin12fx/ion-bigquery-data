<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;
use App\Http\Controllers\Api\UserController;
use App\Http\Controllers\Api\Admin\AdminUserController;
/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

/** LOGIN REGISTRATION API **/
Route::post('login', [UserController::class, 'login'])->name('login');
Route::post('logout', [UserController::class, 'logout']);

/** API routes for admin user */
Route::resource('admin-user', 'Api\Admin\AdminUserController');
Route::get('user-roles', [AdminUserController::class, 'userRoles']);

/** API routes for admin role */
Route::resource('admin-role', 'Api\Admin\AdminRoleController');

/** API routes for Properties */
Route::resource('property', 'Api\Admin\PropertyController');




// Route::middleware(['auth:api', 'is_admin'])->group(function () {

    // Route::resource('admin',[AdminController::class,'logout'])->name('admin.logout');


    // Route::get('users',[UserController::class,'index'])->middleware(['auth:api','is_admin'])->name('users.get');
    // Route::put('/user/{id}',[UserController::class,'update'])->middleware(['auth:api','is_admin']);
    // Route::delete('/user/{id}',[UserController::class,'destroy'])->middleware(['auth:api','is_admin']);
// });

